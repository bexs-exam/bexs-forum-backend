from bottle import request, response
from bottle import post
from services.answers import AnswersValidator, AnswersQuery
from services.responses import Success, Error


@post('/answer')
def creation_handler():
    """Handles answer creation"""
    response.headers['Access-Control-Allow-Origin'] = '*'
    try:
        request_data = request.json
        obj, valid = AnswersValidator.validate(request_data)
        if valid:
            if AnswersQuery().insert_answer(request_data):
                return Success().response(request_data)
            return Error().response('Falha ao cadastrar a pergunta')
        else:
            return Error().response(obj.errors)
    except TypeError as e:
        return Error().response('Nenhum dado enviado')
