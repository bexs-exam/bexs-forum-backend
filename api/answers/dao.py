from api.connection import Database


class Dao:
    table = 'questions'

    def __init__(self):
        Database().get_connection()

    def select_target_question(self, target_id: int):
        return Database().get_by_id(self.table, target_id)

    def insert_answer(self, values: str, target_id: int):
        return Database().update(self.table, values, target_id)

