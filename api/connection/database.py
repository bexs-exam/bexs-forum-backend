import mysql.connector as mariadb
from api.utils import Singleton
import ast
import json


@Singleton
class Database:
    connection = None
    user = 'cqwzftbvydu5h0nw'
    password = 'u6lkb4t1vr9b0ctd'
    database = 'rdnibndo507lzg34'
    host = 'un0jueuv2mam78uv.cbetxkdyhwsb.us-east-1.rds.amazonaws.com'
    schema = 'rdnibndo507lzg34'

    def get_connection(self):
        if self.connection is None:
            self.connection = mariadb.connect(
                user=self.user,
                password=self.password,
                database=self.database,
                host=self.host
            )
        return self.connection

    def insert(self, table: str, fields: list, values: str):
        try:
            fields_str = ','.join(fields)

            query = "INSERT INTO {schema}.{table} ({fields_str}) VALUES ({values_str})".format(
                schema=self.schema, table=table, fields_str=fields_str, values_str=values)

            cursor = self.connection.cursor()
            cursor.execute(query)
            self.connection.commit()
            cursor.close()
            return cursor.lastrowid

        except mariadb.Error:
            return False

    def get_all(self, table: str) -> list:
        query = "SELECT * FROM {schema}.{table}".format(schema=self.schema, table=table)
        cursor = self.connection.cursor()
        cursor.execute(query)
        columns = [column[0] for column in cursor.description]
        results = []
        for row in cursor.fetchall():
            dict = {
                columns[0]:  row[0],
                columns[1]:  row[1],
                columns[2]:  row[2],
                columns[3]: json.loads(row[3])
            }
            results.append(dict)

            # results.append(dict(zip(columns, row)))
        return results

    def get_by_id(self, table: str, target_id: int) -> dict:
        query = "SELECT * FROM {schema}.{table} where id={target_id}".format(schema=self.schema, table=table,
                                                                             target_id=target_id)
        cursor = self.connection.cursor()
        cursor.execute(query)
        columns = [column[0] for column in cursor.description]
        results = {}
        for row in cursor.fetchall():
            results = dict(zip(columns, row))
        return results

    def update(self, table: str, field_values: str, target_id: int) -> bool:
        try:
            query = "UPDATE {schema}.{table} set {field_values} where id={target_id}".format(
                schema=self.schema,
                table=table,
                field_values=field_values,
                target_id=target_id
            )
            cursor = self.connection.cursor()
            cursor.execute(query)
            self.connection.commit()
            cursor.close()
            return True
        except mariadb.Error as e:
            print(e)
            return False

    def _recursive_dict_eval(self, old_dict):
        new_dict = old_dict.copy()
        for key, value in old_dict.items():
            try:
                evaled_value = ast.literal_eval(value)
                assert isinstance(evaled_value, dict)
                new_dict[key] = self._recursive_dict_eval(evaled_value)

            except (SyntaxError, ValueError, AssertionError):
                # SyntaxError, ValueError are for the literal_eval exceptions
                pass
        return new_dict
