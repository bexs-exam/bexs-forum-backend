from api.connection import Database


class Dao:
    fields = [
        'text',
        'user',
        'answers'
    ]
    table = 'questions'

    def __init__(self):
        Database().get_connection()

    def insert_question(self, values: str) -> bool:
        return Database().insert(self.table, self.fields, values)

    def get_all_questions(self) -> list:
        return Database().get_all(self.table)
