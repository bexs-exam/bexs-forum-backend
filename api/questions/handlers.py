from bottle import request, response
from bottle import post, get
from services.question import QuestionsValidator, QuestionsQuery
from services.responses import Success, Error



@post('/questions')
def creation_handler():
    """Handles question creation"""
    response.headers['Access-Control-Allow-Origin'] = '*'
    try:
        request_data = request.json
        obj, valid = QuestionsValidator.validate(request_data)
        if valid:
            inserted = QuestionsQuery().insert_question(request_data)
            if inserted:
                request_data['id'] = inserted
                return Success().response(request_data)
            return Error().response('Falha ao cadastrar a pergunta')
        else:
            return Error().response(obj.errors)
    except TypeError:
        return Error().response('Nenhum dado enviado')


@get('/questions')
def listing_handler():
    """Handles question listing"""
    response.headers['Access-Control-Allow-Origin'] = '*'
    all_questions = QuestionsQuery().get_questions()
    return Success().response(all_questions)
