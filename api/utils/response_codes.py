from enum import Enum


class ResponseCode(Enum):
    SUCCESS = 200
    BAD_GATEWAY = 400
    INTERNAL_ERROR = 500
