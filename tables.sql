create table questions
(
    id      int auto_increment
        primary key,
    text    text                         null,
    user    varchar(100)                 null,
    answers longtext collate utf8mb4_bin null,
    constraint answers
        check (json_valid(`answers`))
);
