import unittest
from services.question import QuestionsValidator
from services.answers import AnswersValidator
from services.responses import Error, Success
import json
from bottle import response
from services.question.query import QuestionsQuery


class ValidationQuestionsTestCase(unittest.TestCase):
    valid_data = {
        "answers": [],
        "text": "My answer",
        "user": "another.username"
    }

    invalid_data = {
        "answers": 0,
        "text": 12333,
        "user": 12333
    }

    def setUp(self) -> None:
        self.validator = QuestionsValidator

    def test_valid(self):
        obj, valid = self.validator.validate(self.valid_data)
        self.assertEqual(valid, True)

    def test_invalid(self):
        obj, valid = self.validator.validate(self.invalid_data)
        self.assertEqual(valid, False)


class ValidationAnswersTestCase(unittest.TestCase):
    valid_data = {
        "parent_id": 1,
        "text": "My answer",
        "user": "another.username"
    }

    invalid_data = {
        "parent_id": [],
        "text": 12333,
        "user": 12333
    }

    def setUp(self) -> None:
        self.validator = AnswersValidator

    def test_valid(self):
        obj, valid = self.validator.validate(self.valid_data)
        self.assertEqual(valid, True)

    def test_invalid(self):
        obj, valid = self.validator.validate(self.invalid_data)
        self.assertEqual(valid, False)


class ResponseTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.success = Success()
        self.error = Error()

    def test_success_body(self):
        success_response = json.loads(self.success.response('Success'))
        self.assertEqual(success_response['status'], 'success')

    def test_error_body(self):
        error_response = json.loads(self.error.response('Error'))
        self.assertEqual(error_response['status'], 'error')

    def test_response_status_success(self):
        self.success.response('Success')
        self.assertEqual(response.status_code, 200)

    def test_response_status_error(self):
        self.error.response('Error')
        self.assertEqual(response.status_code, 400)

# class ConnectionTestCase(unittest.TestCase):
#     def setUp(self) -> None:
#         self.database = Database()
#
#     def test_singleton_connection(self):
#         conn1 = self.database.get_connection()
#         conn2 = self.database.get_connection()
#         self.assertEqual(conn1, conn2)


if __name__ == '__main__':
    unittest.main()
