# Aplicação Forum

Api utilizada para cadastro de perguntas e repostas do Forum

# Observações

Esta api foi construida em Python com o auxídio do Framework Bottle, atualmente ela se econtra hospeda no heroku podendo ser acessada por este endereço: https://back-end-test-bexs.herokuapp.com/

### Banco de dados:
O banco de dados da aplicação também está hospedado no heroku, então não é necessário rodar o DB localmente.

# Como executar a aplicação localmente
Siga os passos a baixo para executar a aplicação localmente:

## 1. Faça o clone deste repositório em seu ambiente de desenvolvimento

    $ git clone https://gitlab.com/bexs-exam/bexs-forum-backend.git

## 2. Crie um virtual env para o projeto

Escolha a ferramenta de sua preferencia para utilizar e criar um virtual env para o projeto, minha preferencia é o próprio **virtualenv**:

    $ virtualenv env


## 3. Ative o ambiente de desenvolvimento

    $ source env/bin/activate

## 4. Instale as dependências do projeto

    $ pip install -r requirements.txt

## 5. Execute o projeto
    $ python main.py

# Testes
Utilizei o **nose** nos testes então para executá-los, basta digitar o seguinte comando:

    $ nosetests -v




