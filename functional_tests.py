from webtest import TestApp
import main
import unittest


class QuestionsFunctionalTestCase(unittest.TestCase):
    valid_data = {
        "answers": [],
        "text": "My answer",
        "user": "another.username"
    }

    invalid_data = {
        "answers": 0,
        "text": 12333,
        "user": 12333
    }

    def setUp(self) -> None:
        self.app = TestApp(main.app)

    def test_list_questions(self):
        self.assertEqual(self.app.get('/questions').status, '200 OK')

    def test_create_question(self):
        self.assertEqual(self.app.post_json('/questions', self.valid_data).status, '200 OK')

    # def test_fail_create_question(self):
    #     self.assertEqual(self.app.post_json('/questions', self.invalid_data).status, '400 Bad Request')


class AnswersFunctionalTestCase(unittest.TestCase):
    valid_data = {
        "parent_id": 1,
        "text": "My answer",
        "user": "another.username"
    }

    invalid_data = {
        "parent_id": [],
        "text": 12333,
        "user": 12333
    }

    def setUp(self) -> None:
        self.app = TestApp(main.app)

    def test_create_answer(self):
        self.assertEqual(self.app.post_json('/answer', self.valid_data).status, '200 OK')




