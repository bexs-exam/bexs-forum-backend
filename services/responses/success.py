from bottle import response
from factorys.response import ResponseFactory
import json
from api.utils import ResponseCode


class Success(ResponseFactory):

    def response(self, body) -> str:
        response.headers['Content-Type'] = 'application/json'
        response.status = ResponseCode.SUCCESS.value
        return json.dumps({
            'status': 'success',
            'response': body
        })

