from bottle import response
from factorys.response import ResponseFactory
import json
from api.utils import ResponseCode


class Error(ResponseFactory):
    def response(self, body) -> str:
        response.headers['Content-Type'] = 'application/json'
        response.status = ResponseCode.BAD_GATEWAY.value
        return json.dumps({
            'status': 'error',
            'motives': body
        })
