from api.questions.dao import Dao


class QuestionsQuery:
    @staticmethod
    def insert_question(request_data: dict) -> bool:
        return Dao().insert_question(
            "'{msg_text}', '{msg_username}', '{answers}'".format(
                msg_text=request_data['text'],
                msg_username=request_data['user'],
                answers=request_data['answers']
            )
        )

    @staticmethod
    def get_questions() -> list:
        return Dao().get_all_questions()

