from cerberus import Validator


class QuestionsValidator:

    @staticmethod
    def validate(fields: dict) -> (Validator, bool):
        schema = {
            "text": {"type": "string", "nullable": False, "required": True},
            "user": {"type": "string", "nullable": False, "required": True},
            "answers": {"type": "list", "nullable": True}
        }
        validator = Validator(schema)
        validator.validate(fields)
        return validator, validator.validate(fields)



