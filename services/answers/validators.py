from cerberus import Validator


class AnswersValidator:
    @staticmethod
    def validate(fields: dict) -> (Validator, bool):
        schema = {
            "parent_id": {"type": "integer", "nullable": False, "required": True},
            "text": {"type": "string", "nullable": False, "required": True},
            "user": {"type": "string", "nullable": False, "required": True},
        }
        validator = Validator(schema)
        validator.validate(fields)
        return validator, validator.validate(fields)



