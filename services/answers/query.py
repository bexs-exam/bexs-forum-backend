from api.answers.dao import Dao
import json
from datetime import datetime


class AnswersQuery:
    @staticmethod
    def insert_answer(request_data: dict):
        target_id = request_data['parent_id']
        target_question = Dao().select_target_question(target_id)
        request_data['creationDate'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        del(request_data['parent_id'])
        answers = json.loads(target_question['answers'])
        answers.append(request_data)
        str_values = " {key}='{value}'".format(key='answers', value=json.dumps(answers))
        return Dao().insert_answer(
            str_values,
            target_id
        )

