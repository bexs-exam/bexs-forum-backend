from abc import ABCMeta, abstractmethod


class ResponseFactory(metaclass=ABCMeta):

    @abstractmethod
    def response(self, body):
        pass
