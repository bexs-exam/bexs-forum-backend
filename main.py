import bottle
from bottle import hook, route, response, template

from api.questions import handlers
from api.answers import handlers

app = application = bottle.default_app()

_allow_origin = '*'
_allow_methods = 'PUT, GET, POST, DELETE, OPTIONS'
_allow_headers = 'Authorization, Origin, Accept, Content-Type, X-Requested-With'


@hook('after_request')
def enable_cors():
    '''Add headers to enable CORS'''

    response.headers['Access-Control-Allow-Origin'] = _allow_origin
    response.headers['Access-Control-Allow-Methods'] = _allow_methods
    response.headers['Access-Control-Allow-Headers'] = _allow_headers


@route('/', method='OPTIONS')
@route('/<path:path>', method='OPTIONS')
def options_handler(path=None):
    return


@route('/', method='GET')
def doc_handler():
    return template('redoc')


def run_server():
    bottle.run(reloader=True, debug=True, port=8010)


if __name__ == '__main__':
    run_server()
